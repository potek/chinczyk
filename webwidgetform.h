#ifndef WEBWIDGETFORM_H
#define WEBWIDGETFORM_H

#include <QWidget>
#include <QWebView>
#include <kliker.h>
#include <QPoint>
#include "positionpoint.h"

namespace Ui {
class WebWidgetForm;
}

class WebWidgetForm : public QWidget
{
    Q_OBJECT

public:
    explicit WebWidgetForm(QWidget *parent = 0);
    ~WebWidgetForm();

    QWebView *webView;

    void kliknij(int x, int y);
    void kliknij(QPoint p);

private:
    Ui::WebWidgetForm *ui;
    Kliker klik;

    void readColorAt(int row, int col);

public slots:
    void canvasClick();
    void clickAtPos(PositionPoint *pp);
    void clickAtPos(QObject *pp);

};

#endif // WEBWIDGETFORM_H
