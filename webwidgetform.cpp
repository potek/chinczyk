#include "webwidgetform.h"
#include "ui_webwidgetform.h"

#include <QWebFrame>
#include <QWebElement>
#include <QDebug>
#include <QWebElementCollection>

#include <QMouseEvent>

#include <QCursor>
#include <QApplication>
#include <QDesktopWidget>

WebWidgetForm::WebWidgetForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::WebWidgetForm)
{
    ui->setupUi(this);
    webView = new QWebView(this);

    QUrl qu("http://kurnik.pl");
    webView->load(qu);

    ui->verticalLayout->insertWidget(0, webView);
    webView->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    setMouseTracking(true);
}

WebWidgetForm::~WebWidgetForm()
{
    delete ui;
}

void WebWidgetForm::kliknij(int x, int y)
{
    kliknij(QPoint(x,y));
}

void WebWidgetForm::kliknij(QPoint p)
{
    QMouseEvent *ev = new QMouseEvent(QEvent::MouseButtonPress, p,
                                     Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
    QCoreApplication::sendEvent(webView, ev);
    QMouseEvent *ev2 = new QMouseEvent(QEvent::MouseButtonRelease, p,
                                    Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
    QCoreApplication::sendEvent(webView, ev2);
}

//TODO
void WebWidgetForm::readColorAt(int row, int col)
{

    QWebFrame *frame = webView->page()->mainFrame();
    QWebElement document = frame->documentElement();
    QWebElement canvas = document.findFirst("canvas");

    int xx, yy, wid, hei;
    int diff;

    QPoint base; // global

    if(!canvas.isNull() ) {
      qDebug() << "canvas nie null";
      canvas.geometry().getRect(&xx, &yy, &wid, &hei);
      qDebug() << "x y szer wys" << xx << yy << wid << hei;

      diff = wid/11.5;
      xx += diff* col;
      yy += diff* row+10;

      base = this->mapToGlobal(canvas.geometry().topLeft());
      base += QPoint(xx, yy);

      //QCursor::setPos(base);
      //klik.kliknij(webView, xx, yy); //local

    }

    QPixmap pixmap = QPixmap::grabWidget(this);
       // QRgb rgba = pixmap.toImage().pixel(e->x(), e->y());

    QRgb rgba = pixmap.toImage().pixel(QPoint(xx,yy));

    QString redComponent 	= QString::number(qRed	(rgba));
    QString greenComponent 	= QString::number(qGreen(rgba));
    QString blueComponent 	= QString::number(qBlue	(rgba));

    ui->label->setText("red: " + redComponent+
                       "green: " + greenComponent+
                 "blue: " + blueComponent);
}

void WebWidgetForm::canvasClick()
{
    qDebug() << "elo";
    QWebFrame *frame = webView->page()->mainFrame();
    QWebElement document = frame->documentElement();
    QWebElement canvas = document.findFirst("canvas");

    if(!canvas.isNull() ) {
      qDebug() << "nie null";
      qDebug() << canvas.geometry() << canvas.geometry().topLeft();

      QCursor::setPos(this->mapToGlobal(canvas.geometry().bottomLeft()));



    }
    else {
        qDebug() << "nie ma canvas";
    }

//    qDebug() << ev->isAccepted();
    qDebug() << QCursor::pos();
  //  QCursor::setPos(594,372);

    
}

//TODO przy rozszerzaniu okna
//licz pozycje elementow od środka
void WebWidgetForm::clickAtPos(PositionPoint *pp)
{
    qDebug() << "clickAtPos";

    QWebFrame *frame = webView->page()->mainFrame();
    QWebElement document = frame->documentElement();
    QWebElement canvas = document.findFirst("canvas");

    int xx, yy, wid, hei;
    int diff;

    QPoint base; // global

    if(!canvas.isNull() ) {
      qDebug() << "canvas nie null";
      canvas.geometry().getRect(&xx, &yy, &wid, &hei);
      qDebug() << "x y szer wys" << xx << yy << wid << hei;

      diff = wid/11.5;
      xx += diff* pp->getY();
      yy += diff* pp->getX()+10;

      base = this->mapToGlobal(canvas.geometry().topLeft());
      base += QPoint(xx, yy);

      QCursor::setPos(base);
      klik.kliknij(webView, xx, yy); //local

    }
    else {
        qDebug() << "nie ma canvas" << pp->getPoint() ;
    }  

    readColorAt(pp->getX(), pp->getY());
}

void WebWidgetForm::clickAtPos(QObject *pp)
{
    PositionPoint *p = static_cast<PositionPoint*> (pp);
    clickAtPos(p);
    qDebug() << "clickatpos object" << p->getPoint();
}
