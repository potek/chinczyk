#ifndef CHINCZYK_H
#define CHINCZYK_H

#include <QMainWindow>
#include "webwidgetform.h"
#include <kliker.h>
#include <QSignalMapper>
#include "positionpoint.h"
#include "boardform.h"

#include <QLabel>
#include <QMouseEvent>

namespace Ui {
class Chinczyk;
}

class Chinczyk : public QMainWindow
{
    Q_OBJECT

public:
    explicit Chinczyk(QWidget *parent = 0);
    ~Chinczyk();

    void mousePressEvent(QMouseEvent *e);
    void keyPressEvent(QKeyEvent *e);

signals:
    void clickAtPosSig(QObject*);

private:
    Ui::Chinczyk *ui;

    WebWidgetForm *webForm;
    BoardForm *board;

    QLabel *lab;
};

#endif // CHINCZYK_H
