#include "chinczyk.h"
#include "ui_chinczyk.h"
#include <QDebug>

#include <QLabel>
#include <QDesktopWidget>


Chinczyk::Chinczyk(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Chinczyk)
{
    ui->setupUi(this);
    setMouseTracking(true);
    setEnabled(true);

    lab = new QLabel("a");
    ui->leftWidget->layout()->addWidget(lab);

    //Tworzenie przeglaradki i dodanie do layoutu
    webForm = new WebWidgetForm(this);
    ui->strona->layout()->addWidget(webForm);

    //Guzik canvas TODO:: usun potem
    connect(ui->canvasButton, SIGNAL(clicked()),
            webForm, SLOT(canvasClick()) );

    //Tworzenie planszy i dodanie do layoutu
    board = new BoardForm(this);
    ui->leftWidget->layout()->addWidget(board);

    //Polaczenie planszy z przegladarka
    connect(board->getSignalMapper(), SIGNAL(mapped(QObject*)),
            webForm, SLOT(clickAtPos(QObject*)));
}


void Chinczyk::mousePressEvent(QMouseEvent *e)
{
    QPixmap pixmap = QPixmap::grabWidget(webForm);
    QPixmap originalPixmap = QPixmap::grabWindow(QApplication::desktop()->winId());
   // QRgb rgba = pixmap.toImage().pixel(e->x(), e->y());
    QRgb rgba = originalPixmap.toImage().pixel(e->x(), e->y());

    ui->label->setPixmap(pixmap);
}

void Chinczyk::keyPressEvent(QKeyEvent *e)
{
    if(e->key() == Qt::Key_B) {
        QPixmap pixmap = QPixmap::grabWidget(webForm);
        QPixmap originalPixmap = QPixmap::grabWindow(QApplication::desktop()->winId());
       // QRgb rgba = pixmap.toImage().pixel(e->x(), e->y());

        QRgb rgba = originalPixmap.toImage().pixel(QCursor::pos());



      //  QRgb rgba = palette().brush(QPalette::Background).textureImage().pixel(event->x(),event->y());
      //  qDebug() << palette().brush(QPalette::Background).textureImage().height();

        QString redComponent 	= QString::number(qRed	(rgba));
        QString greenComponent 	= QString::number(qGreen(rgba));
        QString blueComponent 	= QString::number(qBlue	(rgba));

        ui->colorLabel->setText("red: " + redComponent+
                           "green: " + greenComponent+
                     "blue: " + blueComponent);
    }



}



Chinczyk::~Chinczyk()
{
    delete ui;
}
