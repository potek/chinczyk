#-------------------------------------------------
#
# Project created by QtCreator 2014-10-07T16:01:57
#
#-------------------------------------------------

QT       += core gui webkit network webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Chinczyk
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp\
        chinczyk.cpp \
    webwidgetform.cpp \
    kliker.cpp \
    positionpoint.cpp \
    boardform.cpp

HEADERS  += chinczyk.h \
    webwidgetform.h \
    kliker.h \
    positionpoint.h \
    boardform.h

FORMS    += chinczyk.ui \
    webwidgetform.ui \
    boardform.ui
