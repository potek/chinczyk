#ifndef KLIKER_H
#define KLIKER_H

#include <QCoreApplication>
#include <QPoint>


class Kliker
{
public:
    void kliknij(QObject *what, int x, int y);
    void kliknij(QObject *what, QPoint p);
};

#endif // KLIKER_H
