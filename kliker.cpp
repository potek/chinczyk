#include "kliker.h"
#include <QDebug>
#include <QMouseEvent>

void Kliker::kliknij(QObject *what, int x, int y)
{
    QPoint p(x,y);
    kliknij(what, p);
}

void Kliker::kliknij(QObject *what, QPoint p)
{
    QMouseEvent *ev = new QMouseEvent(QEvent::MouseButtonPress, p,
                                   Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);

    QCoreApplication::sendEvent(what, ev);
    QMouseEvent *ev2 = new QMouseEvent(QEvent::MouseButtonRelease, p,
                                   Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
    QCoreApplication::sendEvent(what, ev2);
    qDebug() << "kliknij" << p.x() << p.y();
}
