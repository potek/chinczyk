#ifndef POSITIONPOINT_H
#define POSITIONPOINT_H

#include <QObject>
#include <QPoint>

class PositionPoint : public QObject
{
    Q_OBJECT
public:
    explicit PositionPoint(QObject *parent = 0);
    PositionPoint(int x, int y) : x(x), y(y) {}

    int getX() {return x;}
    int getY() {return y;}
    QPoint getPoint() {return QPoint(x,y);}
signals:

public slots:

private:
    int x;
    int y;

};

#endif // POSITIONPOINT_H
