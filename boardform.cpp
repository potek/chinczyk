#include "boardform.h"
#include "ui_boardform.h"
#include <QLayoutItem>
#include <QDebug>

BoardForm::BoardForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BoardForm)
{
    ui->setupUi(this);
    signalMapper = new QSignalMapper(this);

    setPosPointTab();
    connectPBButtons();
}

BoardForm::~BoardForm()
{
    delete ui;
}

void BoardForm::connectPBButtons()
{
    QLayoutItem *li;
    QPushButton *pb;
    for(int i=0; i < 11; ++i) {
        for(int j=0; j < 11; ++j) {
            li = ui->gridLayout->itemAtPosition(i,j);
            if(li == nullptr) {
                qDebug() << "ERROR connectPBButtons " << i << j << " li is null";
            }
            else {
                pb = dynamic_cast<QPushButton*>(li->widget());
                if(pb == nullptr) {
                    qDebug() << "ERROR connectPBButtons " << i << j << " pb is null";
                }
                else {
                    connect(pb, SIGNAL(clicked()), signalMapper, SLOT(map()));
                    signalMapper->setMapping(pb, ppt[i][j]);
                }
            }
        }
    }
}


void BoardForm::setPosPointTab()
{
    for(int i=0; i < 11; ++i)
        for(int j=0; j < 11; ++j)
            ppt[i][j] = new PositionPoint(i+1,j+1);

}

