#ifndef BOARDFORM_H
#define BOARDFORM_H

#include <QWidget>
#include <QGridLayout>
#include <QSignalMapper>

#include "positionpoint.h"

namespace Ui {
class BoardForm;
}

class BoardForm : public QWidget
{
    Q_OBJECT

public:
    explicit BoardForm(QWidget *parent = 0);
    ~BoardForm();

    QSignalMapper* getSignalMapper() const {return signalMapper; }


private:
    Ui::BoardForm *ui;
    QSignalMapper *signalMapper;
    PositionPoint *ppt[11][11];

    void connectPBButtons();
    void setPosPointTab();
};

#endif // BOARDFORM_H
